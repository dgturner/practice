#include <iostream>
#include <string>

#include "stack.h"

// Public

Stack::Stack() {
    dll = new Dll();
}

Stack::~Stack() {
    delete dll;
}

int Stack::Push(int newValue) {
    try {
        dll->SetAt(0, newValue);
        return dll->GetAt(0);
    }
    catch (string &errMsg) {
        throw errMsg;
    }
}

int Stack::Pop() {
    if (dll->GetSize() == 0)
        throw string("Stack is empty");

    try {
        return dll->DeleteAt(0);
    }
    catch (string &errMsg) {
        throw errMsg;
    }
}

int Stack::GetSize() {
    return dll->GetSize();
}

void Stack::PrintStack() {
    cout << "Top of Stack > ";
    dll->PrintDll();
}