#ifndef __QUEUE__
#define __QUEUE__

#include <iostream>
#include "dll.h"

using namespace std;

class Queue {
    private:
        Dll* dll;
    public:
        Queue();
        ~Queue();

        int Enqueue(int val);
        int Dequeue();
        int GetSize();

        void PrintQueue();

        void Empty();
};

#endif