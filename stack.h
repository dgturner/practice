#ifndef __STACK__
#define __STACK__

#include <string>
#include "dll.h"

using namespace std;

class Stack {
    private:
        Dll* dll;
    public:
        Stack();
        ~Stack();

        int Push(int newValue);
        int Pop();

        int GetSize();

        void PrintStack();
};

#endif