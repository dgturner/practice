ORDERS= g++ -std=c++11 -Wall
OBJECTS= redblacknode.o quicksort.o mergesort.o queue.o stack.o dll.o class pointers a.out node.o nodetest testdll teststack testqueue testmergesort testquicksort

all: redblacknode.o

classes:
	clear
	g++ -Wall classes.cpp -o class
	class

pointers:
	clear
	$(ORDERS) pointers.cpp -o pointers
	pointers

redblacknode.o: node.o
	$(ORDERS) -c redblacknode.cpp

quicksort: quicksort.o
	$(ORDERS) quicksort.o testquicksort.cpp -o testquicksort

quicksort.o:
	$(ORDERS) -c quicksort.cpp

mergesort: mergesort.o
	$(ORDERS) mergesort.o testmergesort.cpp -o testmergesort
	testmergesort

mergesort.o:
	$(ORDERS) -c mergesort.cpp

queue: queue.o
	$(ORDERS) node.o dll.o queue.o testqueue.cpp -o testqueue

queue.o: dll.o 
	$(ORDERS) -c queue.cpp

stack: stack.o
	$(ORDERS) node.o dll.o stack.o teststack.cpp -o teststack

stack.o: dll.o
	$(ORDERS) -c stack.cpp

dll: dll.o
	$(ORDERS) dll.o node.o testdll.cpp -o testdll

dll.o: node.o dll.h
	$(ORDERS) -c dll.cpp

node: node.o
	$(ORDERS) node.o testnode.cpp -o nodetest
	nodetest

node.o: dll.h
	$(ORDERS) -c node.cpp

clean:
	rm -f $(OBJECTS)
