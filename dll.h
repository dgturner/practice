#ifndef __DLL__
#define __DLL__

#include <string>
#include "node.h"

using namespace std;

class Dll {
	private:
		Node *head;
		Node *tail;
		int size;

		Node* GetFromBack(int index);
		Node* GetFromFront(int index);
		Node* GetNodeAt(int index);
		Node *SetHead(Node *newHead);
		Node *SetTail(Node *newTail);
		Node *GetHead();
		Node *GetTail();
	public:
		Dll();
		~Dll();

		int GetAt(int index);
		int GetSize();
		int SetAt(int index, int value);

		Dll* Append(Dll *toAppend);

		int DeleteAt(int index);
		void PrintDll();

};

#endif
