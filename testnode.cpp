#include "node.h"
#include <iostream>

using namespace std;

int main(void) {
	
	Node* node1 = new Node();
	node1->SetValue(6);
	cout << "This should be 6: " << node1->GetValue() << endl;
	Node* node2 = new Node();
	node2->SetValue(7);
	node2->SetNextNode(node1);
	node1->SetPreviousNode(node2);
	cout << "Should be " << node2->GetValue() << ": " << node1->PreviousNode()->GetValue() << endl;
	cout << "Should be " << node1->GetValue() << ": " << node2->NextNode()->GetValue() << endl;
	try {
		node1->NextNode();
	}
	catch (string &errMsg) {
		cout << errMsg << endl;
	}
	try {
		node2->PreviousNode();
	}
	catch (string &errMsg) {
		cout << errMsg << endl;
	}
	Node *node3 = new Node(69);
	cout << "Should be 69: " << node3->GetValue() << endl;

	return 0;
}
