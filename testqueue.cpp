#include <iostream>
#include <string>
#include "queue.h"

using namespace std;

int main(void) {
    Queue *q = new Queue();
    q->Enqueue(16);
    q->Enqueue(32);
    q->Enqueue(48);
    q->Enqueue(64);

    q->PrintQueue();

    cout << "Dequeued value " << q->Dequeue() << endl;
    q->PrintQueue();
    // q->Dequeue();
    // q->Dequeue();
    // q->Dequeue();
    // q->PrintQueue();
    // try {
    //     q->Dequeue();
    // }
    // catch (string &errMsg) {
    //     cout << errMsg << endl;
    // }
    delete q;
    return 0;
}