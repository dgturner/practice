#include "node.h"
#include <string>
#include <iostream>

using namespace std;

Node::Node() : next(0), previous(0), value(0) {
}

Node::Node(int newValue) : next(0), previous(0), value(newValue) {
}

Node::~Node() {
	next = 0;
	previous = 0;
	cout << value << " deleted" << endl;
}

Node* Node::GetNextNode() {
	return next;
}

Node* Node::GetPreviousNode() {
	return previous;
}

void Node::SetNextNode(Node *newNext){
	next = newNext;
	return;
}

void Node::SetPreviousNode(Node *newPrevious){
	previous = newPrevious;
	return;
}

int Node::GetValue() {
	return value;
}

void Node::SetValue(int newValue) {
	value = newValue;
	return;
}
