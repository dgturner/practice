
#include <string>
#include <iostream>
#include "dll.h"

using namespace std;

// Public

Dll::Dll() : head(0), tail(0), size(0) {
}

Dll::~Dll() {
	Node *traverse = head;
	while (traverse != 0) {
		Node *temp = traverse->GetNextNode();
		delete traverse;
		traverse = temp;
	}
}

int Dll::GetAt(int index) {
	try {
		return GetNodeAt(index)->GetValue();
	}
	catch(string &errMsg) {
		throw errMsg;
	}
}

int Dll::GetSize() {
	return size;
}

int Dll::SetAt(int index, int value) {
	if (index > size)
		throw string("Index out of range (index > size)");
	else if (index < 0)
		throw string("Index out of range (index < 0)");

	Node* newNode = new Node(value);
	if (size == 0) {
		SetHead(newNode);
		SetTail(newNode);
	}
	else if (index == 0) {
		newNode->SetNextNode(GetHead());
		GetHead()->SetPreviousNode(newNode);
		SetHead(newNode);
	}
	else if (index == size) {
		newNode->SetPreviousNode(GetTail());
		GetTail()->SetNextNode(newNode);
		SetTail(newNode);
	}
	else {
		Node* nodeToReplace;
		try {
			nodeToReplace = GetNodeAt(index);
		}
		catch (string &errMsg) {
			throw errMsg;
		}

		newNode->SetNextNode(nodeToReplace);
		newNode->SetPreviousNode(nodeToReplace->GetPreviousNode());
		
		nodeToReplace->GetPreviousNode()->SetNextNode(newNode);
		nodeToReplace->SetPreviousNode(newNode);
	}

	size++;
	return newNode->GetValue();
}

int Dll::DeleteAt(int index) {
	Node* toDelete;
	try {
		toDelete = GetNodeAt(index);
	}
	catch (string &errMsg) {
		throw errMsg;
	}

	Node* previousNode = toDelete->GetPreviousNode();
	Node* nextNode = toDelete->GetNextNode();

	if (toDelete != head)
		previousNode->SetNextNode(nextNode);
	if (toDelete != tail)
		nextNode->SetPreviousNode(previousNode);

	if (toDelete == head)
		head = nextNode;
	if (toDelete == tail)
		tail = previousNode;
	
	int valueToReturn = toDelete->GetValue();
	delete toDelete;

	size--;
	return valueToReturn;
}

void Dll::PrintDll() {
	if (size == 0) {
		cout << "Empty" << endl;
		return;
		}
	Node* traverse = GetHead();
	while (traverse != 0) {
		cout << traverse->GetValue() << " ";
		traverse = traverse->GetNextNode();
	}
	cout << endl;
}

Dll* Dll::Append(Dll* toAppend) {
	Node *tail = GetTail();
	Node *oldHead = toAppend->GetHead();
	Node *newTail = toAppend->GetTail();

	tail->SetNextNode(oldHead);
	oldHead->SetPreviousNode(tail);
	SetTail(newTail);
	size += toAppend->GetSize();

	toAppend->SetHead(0);
	toAppend->SetTail(0);
	toAppend->size = 0;

	return this;
}


// Private

Node* Dll::GetHead() {
	return head;
}

Node* Dll::GetTail() {
	return tail;
}

Node* Dll::GetNodeAt(int index){
	if (size == 0)
		throw string("Dll is empty");
	else if (index >= size)
		throw string("Index out of range (index >= size)");
	else if (index < 0)
		throw string("Index out of range (index < 0)");


	if (index > (size/2))
		return GetFromBack(index);
	else
		return GetFromFront(index);
}

Node* Dll::GetFromBack(int index){
	Node* traverse = tail;
	int steps = size - index - 1;
	
	try {
		while (steps != 0) {
			traverse = traverse->GetPreviousNode();
			--steps;
		}
	}
	catch (string &errMsg) {
		cout << errMsg << endl;
		throw string("Ending procedure");
	}
	
	return traverse;
}

Node* Dll::GetFromFront(int index) {
	Node* traverse = head;
	int steps = 0;
	try {
		while (steps != index) {
			traverse = traverse->GetNextNode();
			steps++;
		}
	}
	catch (string &errMsg) {
		cout << errMsg << endl;
		throw string("Ending Procedure");
	}
	return traverse;
}

Node* Dll::SetHead(Node* newHead) {
	head = newHead;
	return head;
}

Node* Dll::SetTail(Node* newTail) {
	tail = newTail;
	return tail;
}