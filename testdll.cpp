#include <iostream>
#include <assert.h>
#include "dll.h"

using namespace std;

int main(void) {
	Dll *dll1 = new Dll();
	for (int i = 0; i < 12; i++)
		dll1->SetAt(dll1->GetSize(), i);
	dll1->PrintDll();
	dll1->SetAt(8, 69);
	dll1->PrintDll();
	int deletedValue = dll1->DeleteAt(dll1->GetSize()-1);
	cout << "deletedValue: " << deletedValue << endl;
	dll1->PrintDll();
	delete dll1;
	dll1 = new Dll();	
	try {
		dll1->GetAt(100);
	}
	catch(string &errMsg) {
		cout << errMsg << endl;
	}
	dll1->SetAt(0, 12);
	try {
		dll1->DeleteAt(100);
	}
	catch(string &errMsg) {
		cout << errMsg << endl;
	}
	dll1->PrintDll();
	return 0;
}
