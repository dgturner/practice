#include <iostream>
#include <string>
#include "dll.h"
#include "queue.h"

Queue::Queue() : dll(new Dll()) { }
Queue::~Queue() {
    cout << "Queue Destructor called" << endl;
    delete dll;
    cout << "Queue Destructor finished" << endl;
}

int Queue::Enqueue(int newValue) {
    try {
        return dll->SetAt(0, newValue);
    }
    catch(string &errMsg) {
        throw errMsg;
    }
}

int Queue::Dequeue() {
    try {
        return dll->DeleteAt(dll->GetSize() - 1);
    }
    catch (string &errMsg) {
        throw errMsg;
    }
}

int Queue::GetSize() {
    return dll->GetSize();
}

void Queue::Empty() {
    delete dll;
    dll = new Dll();
}

void Queue::PrintQueue() {
    dll->PrintDll();
}