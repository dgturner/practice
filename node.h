#ifndef __NODE__
#define __NODE__
#include <string>

using namespace std;

class Node {
	private:
		Node *next;
		Node *previous;
		int value;
	public:
		Node();
		Node(int newValue);
		~Node();
		Node *GetNextNode();
		Node *GetPreviousNode();
		void SetNextNode(Node *newNext);
		void SetPreviousNode(Node *newPrevious);
		int GetValue();
		void SetValue(int newValue);
};
#endif
