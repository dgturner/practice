#include <iostream>
#include <string>

#include "stack.h"

using namespace std;

int main(void) {
    Stack* stack = new Stack();
    for (int i = 0; i < 100; i++)
        stack->Push(i);
    stack->PrintStack();
    for (int i = 0; i < 100; i++)
        cout << "Deleted: " << stack->Pop() << endl;
    for (int i = 0; i < 100; i++)
        stack->Push(i);
    try {
        stack->Pop();
    }
    catch (string &errMsg) {
        cout << errMsg << endl;
    }
    delete stack;
}